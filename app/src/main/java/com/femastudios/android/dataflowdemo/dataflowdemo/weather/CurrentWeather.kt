package com.femastudios.android.dataflowdemo.dataflowdemo.weather

import java.time.LocalTime
import java.time.ZoneOffset

/**
 * Class that keeps the current weather information
 *
 * @param description a descriptive status of the weather
 * @param temperature current temperature, in celsius
 * @param pressure current pressure, in hPa
 * @param humidity current humidity, in percentage
 * @param visibility current visibility, in meters. Optional
 * @param windSpeed current wind speed, in km/h
 * @param windDegree current wind degree
 * @param cloudiness current cloudiness, in percentage
 * @param sunrise sunrise at the location
 * @param sunset sunset at the location
 * @param timezone the zone offset of the location
 * @param cityName the name of the city, or `null` if non-existent
 */
class CurrentWeather(
    val description: String,
    val temperature: Double,
    val pressure: Int,
    val humidity: Int,
    val visibility: Int?,
    val windSpeed: Double,
    val windDegree: Int,
    val cloudiness: Int,
    val sunrise: LocalTime,
    val sunset: LocalTime,
    val timezone: ZoneOffset,
    val cityName: String?
)