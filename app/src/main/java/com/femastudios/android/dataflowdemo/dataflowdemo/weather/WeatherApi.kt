package com.femastudios.android.dataflowdemo.dataflowdemo.weather

import android.location.Location
import org.json.JSONObject
import java.io.IOException
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL
import java.time.Instant
import java.time.ZoneOffset


/**
 * Utility class that helps us interface with openweathermap APIs
 */
object WeatherApi {

    /**
     * The API key
     */
    private val API_KEY = "e440c6e682a08e38ac2d48e0e811a2a1"


    /**
     * Calls the API endpoint to obtain the current weather of the given [location]
     *
     * @throws IOException on download error
     */
    fun getCurrentWeather(location: Location): CurrentWeather {
        //Create HTTP request
        val url = URL("https://api.openweathermap.org/data/2.5/weather?lat=${location.latitude}&lon=${location.longitude}&APPID=$API_KEY")
        val conn = url.openConnection() as HttpURLConnection
        conn.requestMethod = "GET"

        return if (conn.responseCode == 200) {
            //Request completed successfully, parse response as JSON
            val json = JSONObject(InputStreamReader(conn.inputStream).use { it.readText() })
            val main = json.getJSONObject("main")

            val timezone = ZoneOffset.ofTotalSeconds(json.getInt("timezone"))

            //Return current weather object
            CurrentWeather(
                description = json.getJSONArray("weather").getJSONObject(0).getString("description"),
                temperature = main.getDouble("temp") - 273.15f,
                pressure = main.getInt("pressure"),
                humidity = main.getInt("humidity"),
                visibility = json.optInt("visibility"),
                windSpeed = json.getJSONObject("wind").getDouble("speed") / 3.6f,
                windDegree = json.getJSONObject("wind").getInt("deg"),
                cloudiness = json.getJSONObject("clouds")?.getInt("all") ?: 0,
                sunrise = Instant.ofEpochSecond(json.getJSONObject("sys").getLong("sunrise")).atOffset(timezone).toLocalTime(),
                sunset = Instant.ofEpochSecond(json.getJSONObject("sys").getLong("sunset")).atOffset(timezone).toLocalTime(),
                timezone = timezone,
                cityName = json.getString("name").ifEmpty { null }
            )
        } else {
            //If response code is not 200, something went wrong... throw IOException
            throw IOException("Response was " + conn.responseCode + ": " + conn.responseMessage)
        }
    }
}