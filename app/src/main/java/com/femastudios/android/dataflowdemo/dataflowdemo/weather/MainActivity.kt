package com.femastudios.android.dataflowdemo.dataflowdemo.weather

import android.Manifest
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.view.Gravity
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.femastudios.android.core.dp
import com.femastudios.android.core.setPadding
import com.femastudios.dataflow.android.declarativeui.*
import com.femastudios.dataflow.android.viewState.extensions.setVisible
import com.femastudios.dataflow.async.Error
import com.femastudios.dataflow.async.FlowStrategy
import com.femastudios.dataflow.async.Loaded
import com.femastudios.dataflow.async.util.async
import com.femastudios.dataflow.extensions.not
import com.femastudios.dataflow.extensions.plus
import com.femastudios.dataflow.extensions.plusF
import com.femastudios.dataflow.util.mutableFieldOf
import com.google.android.gms.location.*
import java.io.IOException

/**
 * Main and only activity where we do most part of our work
 */
class MainActivity : AppCompatActivity() {

    /**
     * Google's object that handles the location
     */
    private lateinit var fusedLocationClient: FusedLocationProviderClient

    /**
     * This MutableField describes the current location. It is null if the current location is unknown or hasn't been obtained yet
     */
    private val currentLocation = mutableFieldOf<Location?>(null)
    /**
     * This attribute transforms the current location field, downloading the appropriate current weather. If the location is null or the API request doesn't work, it throws an error.
     * The flow strategy is consistent because when we press the "Reload" buttton we want this attribute to have a "Loading" status
     */
    private val currentWeather = currentLocation.async().transform(flowStrategy = FlowStrategy.CONSISTENT) { location ->
        if (location == null) {
            throwError("No location yet")
        } else {
            try {
                WeatherApi.getCurrentWeather(location)
            } catch (ioe: IOException) {
                throwError("Error downloading weather data", ioe.message)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //Load the FusedLocationProviderClient
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        //Request the current location
        loadLocation()

        //Add the relevant UI components for this activity
        setContentView(new.linearLayout(vertical = true) {
            setPadding(dp(8))


            //Top text that informs us of the current lat/lon location, if known
            //Notice that we are transformin the currentLocation field to make a readable string
            add.text("Current location:\n".plusF(currentLocation.transform {
                if (it == null) "Unknown" else it.latitude.toString() + "; " + it.longitude
            })) {
                textSize = 22f
                gravity = Gravity.CENTER
            }

            add.vSpace(dp(22))

            //Add a circle progress bar that is visible only when the attribute has status "Loading"
            add.circleProgressBar {
                setVisible(currentWeather.isLoading())
            }
            //Add a layout displaying the error that is visible only when the attribute has status "Error"
            add.linearLayout(vertical = true) {
                setVisible(currentWeather.isError())
                //Here we transform the attribute casting its value to Error, since it's the only case when this views will be visible
                val error = currentWeather.asField().transform {
                    it as? Error
                }
                add.text(error.transform { it?.displayableMessage }) {
                    textSize = 22f
                    gravity = Gravity.CENTER
                }
                add.text(error.transform { it?.displayableDescription }) {
                    gravity = Gravity.CENTER
                }
            }
            //Add a layout for displaying the current weather information
            add.linearLayout(vertical = true) {
                setVisible(currentWeather.isLoaded())
                //Here we transform the attribute casting its value to Loaded, since it's the only case when this views will be visible
                val weather = currentWeather.asField().transform {
                    (it as? Loaded)?.value
                }

                //Here we add all the text view containing the current weather data
                add.text(weather.transform { "%.0f".format(it?.temperature) }.toStringF() + "°C") {
                    textSize = 48f
                    gravity = Gravity.CENTER
                }
                add.text(weather.transform { it?.cityName ?: "Unknown city" }) {
                    textSize = 32f
                    gravity = Gravity.CENTER
                }
                add.text(weather.transform { it?.description }) {
                    textSize = 22f
                    gravity = Gravity.CENTER
                }
                add.vSpace(dp(16))
                add.text("Details:") {
                    textSize = 20f
                }
                add.text("Wind: ".plusF(weather.transform { "%.1f".format(it?.windSpeed) + "km/h (" + it?.windDegree + "°)" }))
                add.text("Pressure: ".plusF(weather.transform { it?.pressure.toString() + "hPa" }))
                add.text("Humidity: ".plusF(weather.transform { it?.humidity.toString() + "%" }))
                add.text("Visibility: ".plusF(weather.transform { it?.visibility.toString() + "m" })) {
                    setVisible(weather.transform { it?.visibility != null })
                }
                add.text("Clouds: ".plusF(weather.transform { it?.cloudiness.toString() + "%" }))
                add.text("Sunrise: ".plusF(weather.transform { it?.sunrise.toString() + " (local time)" }))
                add.text("Sunset: ".plusF(weather.transform { it?.sunset.toString() + " (local time)" }))
                add.text("Timezone: ".plusF(weather.transform { it?.timezone.toString() }))
            }

            //Lastly we add a button that triggers the redownload of the current weather data
            //It's visible only when the status of the attribute is either "Error" or "Loaded2
            add {
                wrapHeight()
                wrapWidth()
                gravity = Gravity.CENTER
            }.button("Reload", {
                currentWeather.recompute()
            }) {
                setVisible(!currentWeather.isLoading())
            }
        })
    }

    /**
     * This method checks that the user has given appropriate permissions to access the location.
     * Then it updates the value of the currentLocation field to the latest known location and registers a callback for when the location changes.
     */
    private fun loadLocation() {
        if (
            ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
            ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
        ) {
            //Permissions are OK
            fusedLocationClient.lastLocation.addOnSuccessListener {
                //Set the current last known location
                currentLocation.value = it
            }

            //Register a callback that is called when the user changes position
            //Since we don't need high accuracy, we've set PRIORITY_LOW_POWER and smallest displacement at 1km
            fusedLocationClient.requestLocationUpdates(
                LocationRequest.create()
                    .setPriority(LocationRequest.PRIORITY_LOW_POWER)
                    .setSmallestDisplacement(1000f),
                object : LocationCallback() {
                    override fun onLocationResult(lr: LocationResult?) {
                        currentLocation.value = lr?.lastLocation
                    }
                },
                null
            )
        } else {
            //Permissions are not granted, request them...
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION), LOCATION_PERMISSION_REQUEST)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        //Triggered when the permission is either granted or denied
        when (requestCode) {
            LOCATION_PERMISSION_REQUEST -> {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //If the permission was granted, request the location again
                    loadLocation()
                } else {
                    //If the permission was denied, the app cannot work
                    //Inform the user and quit the activity
                    Toast.makeText(this, "Location permission is required to make the app work!", Toast.LENGTH_LONG).show()
                    finish()
                }
            }
        }
    }

    companion object {
        private val LOCATION_PERMISSION_REQUEST = 1
    }
}
