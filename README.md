# Weather
This app is a simple weather app that displays the current weather at the current user location. It demonsrates the usage of attributes.

The app also uses the `declarativeui` library to create its views. 

You can read the comments in the code for more details.

## Screenshot
![Screenshot](https://gitlab.com/femastudios/android/dataflow-examples/dataflowdemo-weather/raw/master/screenshot.jpg)

## Install
You can go to [releases](https://gitlab.com/femastudios/android/dataflow-examples/dataflowdemo-weather/-/releases) to find an APK to install.

